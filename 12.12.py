"""
Написать простой калькулятор с использованием функций.
"""

a, sign, b = input().split()
def calculator(a, b):
    if sign == '+':
        return int(a) + int(b)
    elif sign == '-':
        return int(a) - int(b)
    elif sign == '*':
        return int(a) * int(b)
    elif sign == '/':
        return int(a) / int(b)

print(calculator(a,b))


